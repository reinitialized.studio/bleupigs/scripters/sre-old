--[[
    Ensures Http Requests do not exceed the maxmium budget while auto-retrying failed requests
]]
local RBXHttpService = game:GetService("HttpService")
local RBXPlayers = game:GetService("Players")

local SERVER_STARTED_AT = os.time() -- tell's us when the Server has started. Not sure how accurate this is, 
-- but the goal is to know when the HTTP budget resets

local HttpManager = {prototype = {}}
HttpManager.__index = HttpManager.prototype

function HttpManager.new()
    return setmetatable({
        _budget = 500, 
        _totalRequests = 0,
        _budgetResetsAt = SERVER_STARTED_AT + 60
    }, HttpManager)
end

--[[
    HttpManager:requestAsync(string url, string method, table headers, string body)
    @returns boolean success, string response
    Attempts to perform an Http Request while respecting the HTTP budget
]]
function HttpManager.prototype:requestAsync(url, method, headers, body)
    local success, response
    if self._budget < 39 then
        print("budget is low, waiting ".. (os.time(self._budgetResetsAt))) 
        -- wait until we're no longer exceeding the budget
        wait(os.time() - (self._budgetResetsAt + 1)) 
    end
    for i = 1, (self._budget > 20 and 10 or 3) do
        print(i)
        -- recalculate budget
        if self._budgetResetsAt < os.time() then
            -- budget has reset, adjust accordingly
            print("recalculating budget")
            self._budgetResetsAt = os.time() + 60
            self._requestBudget = #RBXPlayers:GetPlayers() > 2 and 500 or 20
            return self:requestAsync(url, method, headers, body)
        end
        print('executing')
        success, response = pcall(
            RBXHttpService.RequestAsync,
            RBXHttpService,
            {
                Url = url,
                Method = method,
                Headers = headers,
                Body = body
            }
        )
        if success then
            return success, response
        end
        wait(i * 2)
    end

    return success, response
end

return HttpManager.new()