--[[
    Manages communication with the Bleu Pigs backend service
]]
local HttpManager = require(script.Parent:WaitForChild("HttpManager"))
local RBXHttpService = game:GetService("HttpService")

local DEVELOP_BRANCH = 4528018876
local MASTER_BRANCH = 4737604093
local DEVELOP_BASE = "https://api.bleupigs.club/dev/"
local MASTER_BASE = "https://prod.bp.zv.wtf/"
local VALIDATE_ENDPOINT = "auth/roblox/instantiate"
local REFRESH_TOKEN_ENDPOINT = "auth/roblox/refresh" 

local BackendManager = {prototype = {}}
BackendManager.__index = BackendManager.prototype

function BackendManager.new()
    return setmetatable({
    }, BackendManager)
end

function BackendManager.prototype:queryAsync(query, arguments)
    -- validate we are authenticated with the backend
    local success, response = HttpManager:requestAsync(
        MASTER_BASE .. VALIDATE_ENDPOINT,
        "POST",
        {
            ["Content-Type"] = "application/json"
        },
        RBXHttpService:JSONEncode(
            {
                jobId = game.JobId,
                guild = RBXHttpService:GenerateGUID(false),
                placeId = game.PlaceId
            }
        )
    )
    -- validate there wasn't an issue communicating with backend
    if success then
        -- validate backend successfully authenticated the session
        if response.Body.success == "success" then
            -- authentication success, store data
            print("success", response.Body)
        else
            print("failure", response.Body)
        end
    end
    print(success, table.unpack(response))
end

return BackendManager.new()