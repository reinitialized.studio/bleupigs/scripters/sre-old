--[[
    - Validates user authorization and permissions
    - Ensures user data is retrieved/saved
]]

local RBLXPlayers = game:GetService("Players")
local BackendManager = require(script.Parent:WaitForChild("BackendManager"))

local UsersManager = {prototype = {}}
UsersManager.__index = UsersManager.prototype

function UsersManager.new()
    local this = setmetatable({
        _cachedUsers = setmetatable({}, {__mode = "k"})
    }, UsersManager)

    this.userJoining = function(joiningPlayer)
        local user = this._cachedUsers[joiningPlayer.userId]
        if not user then
        end
    end
    this.userLeaving = function(leavingPlayer)

    end

    return this
end