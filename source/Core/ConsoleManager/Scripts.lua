local Scripts = {}

Scripts.NORMAL = 0
Scripts.SAVED = 1
Scripts.RUNNING = 2
Scripts.QUICK = 4

local scripts = {}
local index = {}

local update = Instance.new("BindableEvent")

Scripts.update = update.Event

function Scripts.add(id, name, state)
	assert(index[name] == nil)
	assert(scripts[id] == nil)
	index[name] = id
	scripts[id] = {
		name = name,
		state = state,
	}
	update:Fire("create", id, name, state)
end

function Scripts.list()
	local res = {}
	for id in pairs(scripts) do
		table.insert(res, id)
	end
	return res
end

function Scripts.remove(id)
	index[scripts[id].name] = nil
	scripts[id] = nil
	update:Fire("remove", id)
end

function Scripts.getId(name)
	return index[name]
end

function has(state, mask)
	return bit32.band(state, mask) == mask
end

function Scripts.matches(id, mask)
	return has(scripts[id].state, mask)
end

function Scripts.getName(id)
	return scripts[id].name
end

function Scripts.addState(id, state)
	local currState = scripts[id].state
	local newState = bit32.bor(currState, state)
	scripts[id].state = newState
	update:Fire("state", id, currState, newState, state)
end

function Scripts.removeState(id, state)
	local currState = scripts[id].state
	local newState = bit32.band(currState, bit32.bnot(state))
	scripts[id].state = newState
	update:Fire("state", id, currState, newState, state)
end

_G.Scripts = Scripts

return Scripts