local Console = {}

local ev = Instance.new("BindableEvent")

local data = {}

local scheduledEvent

function schedule()
	if not scheduledEvent then
		scheduledEvent = game:GetService("RunService").RenderStepped:Connect(function()
			ev:Fire(data[#data])
			scheduledEvent:Disconnect()
			scheduledEvent = nil
		end)
	end
end

function Console.print(kind, msg, when)
	local t = data[#data]
	if t and rawequal(msg, t.msg) and kind == t.kind then
		t.count = t.count + 1
		t.when = when
		schedule()
		return
	end
	
	table.insert(data, {
		kind = kind,
		msg = msg,
		count = 1,
		when = when,
	})
	
	schedule()
end

function Console.clear()
	data = {}
	Console.data = data
	
	schedule()
end

Console.data = data

Console.event = ev.Event

_G.Console = Console

return Console