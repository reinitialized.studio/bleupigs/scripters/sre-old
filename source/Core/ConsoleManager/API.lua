local API = {}

local onCommand = Instance.new("BindableEvent")
local onToggleRun = Instance.new("BindableEvent")
local onToggleSave = Instance.new("BindableEvent")

API.onCommand = onCommand.Event
API.onToggleRun = onToggleRun.Event
API.onToggleSave = onToggleSave.Event

function API.runCommand(cmd)
	onCommand:Fire(cmd)
end

function API.toggleRun(id)
	onToggleRun:Fire(id)
end

function API.toggleSave(id)
	onToggleSave:Fire(id)
end

return API