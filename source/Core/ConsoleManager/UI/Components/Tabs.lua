local Sui = require(script.Parent.Parent.Sui)
local Theme = require(script.Parent.Parent.Theme)

local selected
local prevSelected

function Tabs(props, children)
	if not selected then
		selected = props.TabOrder[1]
	end
	local tabs = {}
	local order = {}
	local refs = {}
	local tabContents = {}
	for i, tab in ipairs(props.TabOrder) do
		order[tab] = i
		tabContents[tab] = props[tab]
		table.insert(tabs, {
			ClassName = "TextButton",
			Text = tab,
			Font = Theme.Font,
			LayoutOrder = i,
			TextSize = 20,
			BackgroundTransparency = 1,
			Size = UDim2.new(0, 128, 0, 32),
			TextColor3 = Theme.Foreground,
			[Sui.event.MouseButton1Click] = function(self)
				prevSelected = selected
				selected = tab
				refs.updateSelector()
				refs.updateContent()
			end,
		})
	end
	return Sui(refs) {
		ClassName = "Frame",
		Size = props.Size,
		Position = props.Position,
		BackgroundTransparency = 1,
		
		{
			ClassName = "Frame",
			BackgroundTransparency = 1,
			Size = UDim2.new(1, 0, 0, 32),
			Position = UDim2.new(0, 0, 0, 0),
			Children = tabs,
			
			{
				ClassName = "UIListLayout",
				FillDirection = "Horizontal",
			},
		},
		
		{
			ClassName = "Frame",
			Ref = {
				updateSelector = function(self)
					self:TweenPosition(UDim2.new(0, (order[selected] - 1) * 128 + 16, 0, 32 - 3), nil, nil, 0.15, true)
				end,
			},
			Position = UDim2.new(0, (order[selected] - 1) * 128 + 16, 0, 32 - 3),
			Size = UDim2.new(0, 128 - 32, 0, 3),
			BackgroundColor3 = Theme.Accent,
			BorderSizePixel = 0,
		},
		
		{
			ClassName = "Frame",
			BackgroundTransparency = 1,
			Size = UDim2.new(1, 0, 1, -32),
			Position = UDim2.new(0, 0, 0, 32),
			ClipsDescendants = true,
			Ref = {
				updateContent = function(self)
					tabContents[prevSelected].Parent = nil
					tabContents[selected].Parent = self
				end,
			},
			tabContents[selected],
		},
	}
end

return Tabs