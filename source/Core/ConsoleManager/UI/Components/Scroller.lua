local Theme = require(script.Parent.Parent.Theme)
local Sui = require(script.Parent.Parent.Sui)

function Scroller(props, children)
	for k, v in pairs {
		ClassName = "ScrollingFrame",
		BackgroundTransparency = 1,
		BorderSizePixel = 0,
		ScrollBarThickness = 6,
		ScrollBarImageColor3 = Theme.Foreground,
	} do props[k] = v end
	return Sui(props, children)
end

return Scroller