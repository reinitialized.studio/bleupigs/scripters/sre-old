local Sui = require(script.Parent.Parent.Sui)
local Theme = require(script.Parent.Parent.Theme)

local refs = {}

local function update()
	local Scripts = require(script.Parent.Scripts)
	local sel = Scripts.getSelectedScript()
	if sel == nil then return end
	if not (refs.runStop and refs.save) then return end
	if _G.Scripts.matches(sel, _G.Scripts.RUNNING) then
		refs.runStop.Image.Image = "rbxassetid://5119565450"
		refs.runStop.Label.Text = "Stop"
	else
		refs.runStop.Image.Image = "rbxassetid://5119565234"
		refs.runStop.Label.Text = "Run"
	end
	if _G.Scripts.matches(sel, _G.Scripts.SAVED) then
		refs.save.Image.Image = "rbxassetid://5119620033"
		refs.save.Label.Text = "Unsave"
	else
		refs.save.Image.Image = "rbxassetid://5119558579"
		refs.save.Label.Text = "Save"
	end
end

function ScriptControls(props)
	return Sui(refs) {
		ClassName = "Frame",
		BackgroundColor3 = Theme.Background,
		BorderSizePixel = 0,
		Visible = props.Visible,
		Size = props.Size,
		Position = props.Position,
		ClipsDescendants = true,
		ZIndex = 3,
		
		{
			ClassName = "Frame",
			Size = UDim2.new(1, 0, 0, 1),
			Position = UDim2.new(0, 0, 0, 0),
			BorderSizePixel = 0,
			BackgroundColor3 = Theme.Border,
		},
		
		{
			ClassName = "Action",
			Ref = "runStop",
			Size = UDim2.new(0, 32, 0, 32),
			Position = UDim2.new(0, 12, 0, 12),
			Image = "rbxassetid://5119565234",
			Text = "Run",
			[Sui.event.Click] = function(self)
				local Scripts = require(script.Parent.Scripts)
				local sel = Scripts.getSelectedScript()
				if sel == nil then return end
				require(script.Parent.Parent.Parent.API).toggleRun(sel)
			end,
		},
		
		{
			ClassName = "Action",
			Ref = "save",
			Size = UDim2.new(0, 32, 0, 32),
			Position = UDim2.new(0, 12 + 44 * 1, 0, 12),
			Image = "rbxassetid://5119558579",
			Text = "Save",
			[Sui.event.Click] = function(self)
				local Scripts = require(script.Parent.Scripts)
				local sel = Scripts.getSelectedScript()
				if sel == nil then return end
				require(script.Parent.Parent.Parent.API).toggleSave(sel)
			end,
		},
	}
end

return {
	ScriptControls = ScriptControls,
	update = update,
}
