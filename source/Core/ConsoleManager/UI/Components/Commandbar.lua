local UIS = game:GetService("UserInputService")
local RunService = game:GetService("RunService")
local Sui = require(script.Parent.Parent.Sui)
local Theme = require(script.Parent.Parent.Theme)

function Commandbar(props, children)
	local refs = {}
	UIS.InputBegan:Connect(function(input, gp)
		if gp then return end
		if input.UserInputType == Enum.UserInputType.Keyboard
			and input.KeyCode == Enum.KeyCode.Quote then
			local ins = refs.instance
			RunService.RenderStepped:Wait()
			if ins:IsDescendantOf(game) then
				ins:CaptureFocus()
			end
		end
	end)
	return Sui(refs) {
		ClassName = "Frame",
		Size = props.Size,
		Position = props.Position,
		BackgroundColor3 = Theme.Background,
		BorderColor3 = Theme.Border,
		BorderSizePixel = 1,
		ZIndex = 2,
		
		{
			ClassName = "TextBox",
			Ref = {"instance"},
			BackgroundTransparency = 1,
			PlaceholderColor3 = Theme.Foreground,
			PlaceholderText = "Press (') key or click here to type a command",
			Text = "",
			TextColor3 = Theme.Foreground,
			TextSize = 16,
			Font = Theme.Font,
			TextXAlignment = "Left",
			Size = UDim2.new(1, -4, 1, 0),
			Position = UDim2.new(0, 4, 0, 0),
			ClearTextOnFocus = false,
			[Sui.event.FocusLost] = function(self, enterPressed)
				if enterPressed then
					local txt = self.Text
					self.Text = ""
					require(script.Parent.Parent.Parent.API).runCommand(txt)
				end
			end,
		},
	}
end

return Commandbar