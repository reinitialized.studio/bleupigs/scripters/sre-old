local Sui = require(script.Parent.Parent.Sui)

Sui.component("Tabs", require(script.Parent.Tabs))
Sui.component("DropshadowFrame", require(script.Parent.DropshadowFrame))
Sui.component("Scripts", require(script.Parent.Scripts).Scripts)
Sui.component("Scroller", require(script.Parent.Scroller))
Sui.component("Commandbar", require(script.Parent.Commandbar))
Sui.component("Combobox", require(script.Parent.Combobox))
Sui.component("ScriptControls", require(script.Parent.ScriptControls).ScriptControls)
Sui.component("Action", require(script.Parent.Action))

return true