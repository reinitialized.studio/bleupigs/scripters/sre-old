local Sui = require(script.Parent.Parent.Sui)
local Theme = require(script.Parent.Parent.Theme)

function map(arr, func)
	local res = {}
	for k, v in pairs(arr) do
		res[k] = func(v)
	end
	return res
end

function tokenize(str)
	local res = {}
	local run
	for i = 1, #str do
		local c = str:sub(i, i)
		local b = c:byte()
		if b >= 48 and b <= 48 + 9 then -- if digit
			local digit = b - 48
			if run then
				run = run * 10 + digit
			else
				run = digit
			end
		else
			if run then
				table.insert(res, run)
			end
			table.insert(res, c)
		end
	end
	table.insert(res, run)
	return res
end

function strnatcmp(a, b)
	local at = tokenize(a)
	local bt = tokenize(b)
	for i = 1, math.min(#at, #bt) do
		local ac = at[i]
		local bc = bt[i]
		if ac ~= bc then
			local an = type(ac)
			local bn = type(bc)
			if an == bn then
				if ac < bc then
					return -1
				elseif ac > bc then
					return 1
				end
			elseif an == "number" then
				return -1
			else -- if bn == "number" then
				return 1
			end
		end
	end
	return math.sign(#at - #bt)
end

function blend(back, fore, alpha)
	return Color3.new(
		back.R + alpha * (fore.R - back.R),
		back.G + alpha * (fore.G - back.G),
		back.B + alpha * (fore.B - back.B)
	)
end

local selectedScript

local deselectScript, selectScript

function Scripts(props, children)
	local UI = require(script.Parent.Parent)
	
	local quickscripts = {}
	local scripts = {}
	
	local index = {}
	
	local updateAll
	
	local refs = {}
	
	local add, remove
	
	local function update(id, ig)
		-- lazy way to do it, but it works:
		remove(id, ig)
		add(id, true)
		updateAll()
	end
	
	local tweenId = 0
	
	function deselectScript(ig)
		local me = tweenId + 1
		tweenId = me
		if selectedScript then
			local prev = selectedScript
			selectedScript = nil
			update(prev)
		end
		if not ig then
			UI.scriptControls.Size = UDim2.new(1, -106, 0, 57)
			UI.scriptControls.Position = UDim2.new(0, 106, 1, -23 - 57)
			UI.scriptControls:TweenSizeAndPosition(UDim2.new(1, -106, 0, 0), UDim2.new(0, 106, 1, -23 - 0), nil, nil, 0.2, true)
			coroutine.wrap(function()
				wait(0.2)
				if tweenId == me then
					UI.scriptControls.Visible = false
				end
			end)()
		end
	end
	
	function selectScript(id)
		local me = tweenId + 1
		tweenId = me
		if not selectedScript then
			UI.scriptControls.Visible = true
			UI.scriptControls.Size = UDim2.new(1, -106, 0, 0)
			UI.scriptControls.Position = UDim2.new(0, 106, 1, -23 - 0)
			UI.scriptControls:TweenSizeAndPosition(UDim2.new(1, -106, 0, 57), UDim2.new(0, 106, 1, -23 - 57), nil, nil, 0.2, true)
		end
		deselectScript(true)
		selectedScript = id
		update(id, true)
		require(script.Parent.ScriptControls).update()
	end
	
	function add(id, display)
		local isQuick = _G.Scripts.matches(id, _G.Scripts.QUICK)
		local isRunning = _G.Scripts.matches(id, _G.Scripts.RUNNING)
		local isSaved = _G.Scripts.matches(id, _G.Scripts.SAVED)
		local name = _G.Scripts.getName(id)
		
		local container
		if isQuick then
			container = quickscripts
		else
			container = scripts
		end
		
		local sc = {}
		
		index[id] = sc
		
		sc.id = id
		sc.name = name
		
		local tooltip
		
		function sc.remove()
			sc.btn:Destroy()
			if tooltip then
				tooltip:Destroy()
			end
		end
		
		Sui(sc) {
			ClassName = "TextButton",
			AutoButtonColor = false,
			Parent = display and (isQuick and refs.quickscripts or refs.scripts) or nil,
			Name = name,
			Ref = "btn",
			Text = "",
			BorderSizePixel = 0,
			Size = UDim2.new(1, 0, 0, 20),
			BackgroundColor3 = selectedScript == id and Theme.Accent or Theme.Foreground,
			BackgroundTransparency = selectedScript == id and 0 or 1,
			[Sui.event.MouseButton1Click] = function(self)
				if selectedScript == id then
					deselectScript()
				else
					selectScript(id)
				end
			end,
			[Sui.event.MouseEnter] = function(self)
				self.BackgroundTransparency = selectedScript == id and 0 or Theme.HoverTransparency
				if tooltip then
					tooltip:Destroy()
				end
				if not sc.label.TextFits then
					tooltip = Sui {
						ClassName = "ScreenGui",
						ResetOnSpawn = false,
						ZIndexBehavior = "Sibling",
						DisplayOrder = 2147483647,
						Parent = game:GetService("Players").LocalPlayer:FindFirstChildOfClass("PlayerGui"),
						
						{
							ClassName = "DropshadowFrame",
							ZIndex = 2,
							Size = UDim2.new(0, sc.label.TextBounds.X + 8, 0, 20),
							Position = UDim2.new(0, sc.btn.AbsolutePosition.X, 0, sc.btn.AbsolutePosition.Y),
							BorderSizePixel = 0,
							BackgroundColor3 = blend(Theme.Background, self.BackgroundColor3, 1 - self.BackgroundTransparency),
							ShadowOffset = UDim2.new(0, 0, 0, 2),
							ClipsDescendants = true,
							ShadowScale = 0.3,
							ShadowTransparency = 0.7,
							
							{
								ClassName = "TextLabel",
								Size = UDim2.new(1, -4, 1, 0),
								Position = UDim2.new(0, 4, 0, 0),
								BackgroundTransparency = 1,
								TextColor3 = sc.label.TextColor3,
								TextSize = 18,
								Font = Theme.Font,
								TextXAlignment = "Left",
								Text = sc.label.Text,
							}
						}
					}
				end
			end,
			[Sui.event.MouseLeave] = function(self)
				self.BackgroundTransparency = selectedScript == id and 0 or 1
				if tooltip then
					tooltip:Destroy()
				end
			end,
			
			{
				ClassName = "TextLabel",
				Ref = "label",
				Size = UDim2.new(1, -4, 1, 0),
				Position = UDim2.new(0, 4, 0, 0),
				BackgroundTransparency = 1,
				Text = (isSaved and "~" or "") .. name,
				TextXAlignment = "Left",
				Font = Theme.Font,
				TextSize = 18,
				TextColor3 = selectedScript == id and Theme.AccentForeground or
					Theme.Scripts[(isSaved and "Saved" or "Normal") .. (isRunning and "Running" or "Stopped")],
			}
		}
		
		table.insert(container, sc)
	end
	
	function remove(id, ig)
		if selectedScript == id and not ig then
			deselectScript()
		end
		for _, container in ipairs { quickscripts, scripts } do
			for i, sc in ipairs(container) do
				if sc.id == id then
					table.remove(container, i)
					sc.remove()
					break
				end
			end
		end
	end
	
	function updateAll()
		for _, container in ipairs { quickscripts, scripts } do
			table.sort(container, function(a, b)
				return strnatcmp(a.name, b.name) < 0
			end)
			for i, sc in ipairs(container) do
				sc.btn.LayoutOrder = i
			end
		end
		refs.quickscripts.CanvasSize = UDim2.new(0, 0, 0, #quickscripts * 20)
		refs.scripts.CanvasSize = UDim2.new(0, 0, 0, #scripts * 20)
	end
	
	for _, id in ipairs(_G.Scripts.list()) do
		add(id)
	end
	
	spawn(updateAll) -- ugly hack; see Pages.Console
	
	_G.Scripts.update:Connect(function(kind, id, name, state)
		if kind == "create" then
			add(id, true)
			updateAll()
		end
	end)
	
	_G.Scripts.update:Connect(function(kind, id)
		if kind == "remove" then
			remove(id)
			updateAll()
		end
	end)
	
	_G.Scripts.update:Connect(function(kind, id, prev, now, state)
		if kind == "state" then
			update(id)
		end
	end)
	
	return Sui(refs) {
		ClassName = "Frame",
		Size = props.Size,
		Position = props.Position,
		BackgroundTransparency = 1,
		
		{
			ClassName = "TextLabel",
			Text = "Scripts",
			Font = Theme.Font,
			TextSize = 20,
			BackgroundTransparency = 1,
			TextColor3 = Theme.Foreground,
			Size = UDim2.new(1, 0, 0, 32),
		},
		
		{
			ClassName = "Frame",
			Size = UDim2.new(1, 0, 1, -32),
			Position = UDim2.new(0, 0, 0, 32),
			BackgroundTransparency = 1,
			
			{
				ClassName = "Scroller",
				Ref = "quickscripts",
				Size = UDim2.new(1, 0, 0.5, 0),
				Children = map(quickscripts, function(x) return x.btn end),
				
				{
					ClassName = "UIListLayout",
					SortOrder = "LayoutOrder",
				},
			},
			
			{
				ClassName = "Frame",
				BorderSizePixel = 0,
				BackgroundColor3 = Theme.Border,
				Size = UDim2.new(1, 0, 0, 1),
				Position = UDim2.new(0, 0, 0.5, 0),
			},
			
			{
				ClassName = "Scroller",
				Ref = "scripts",
				Position = UDim2.new(0, 0, 0.5, 1),
				Size = UDim2.new(1, 0, 0.5, -1),
				Children = map(scripts, function(x) return x.btn end),
				
				{
					ClassName = "UIListLayout",
					SortOrder = "LayoutOrder",
				},
			},
		},
	}
end

return {
	Scripts = Scripts,
	getSelectedScript = function()
		return selectedScript
	end,
	setSelectedScript = function(id)
		if selectScript then
			selectScript(id)
		else
			selectedScript = id
		end
	end,
}