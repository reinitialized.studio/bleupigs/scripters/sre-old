local Theme = require(script.Parent.Parent.Theme)
local Sui = require(script.Parent.Parent.Sui)

function Combobox(props)
	local refs = {}
	return Sui(refs) {
		ClassName = "TextButton",
		Ref = "btn",
		BackgroundTransparency = 1,
		Size = props.Size,
		Position = props.Position,
		Text = "",
		AutoButtonColor = false,
		
		[Sui.event.MouseEnter] = function(self)
			refs.arrow.ImageColor3 = Theme.Accent
			refs.border.BackgroundColor3 = Theme.Accent
		end,
		
		[Sui.event.MouseLeave] = function(self)
			refs.arrow.ImageColor3 = Theme.Border
			refs.border.BackgroundColor3 = Theme.Border
		end,
		
		[Sui.event.MouseButton1Click] = function(self)
			refs.arrow.ImageColor3 = Theme.Border
			refs.border.BackgroundColor3 = Theme.Border
			refs.arrow.Rotation = 180
			local menu
			local function close()
				refs.arrow.Rotation = 0
				menu:Destroy()
			end
			local options = {}
			for i, opt in ipairs(props.Options) do
				table.insert(options, Sui {
					ClassName = "TextButton",
					Text = "",
					AutoButtonColor = false,
					BackgroundTransparency = Theme.HoverTransparency,
					BorderSizePixel = 0,
					BackgroundColor3 = Theme.Background,
					Size = UDim2.new(1, 0, 0, 32),
					Position = UDim2.new(0, 0, 0, 4 + (i - 1) * 32),
					[Sui.event.MouseButton1Click] = function()
						close()
						refs.label.Text = opt
						if props[Sui.event.Select] then
							props[Sui.event.Select](refs.btn, opt)
						end
					end,
					[Sui.event.MouseEnter] = function(self)
						self.BackgroundColor3 = Theme.Foreground
					end,
					[Sui.event.MouseLeave] = function(self)
						self.BackgroundColor3 = Theme.Background
					end,
					
					{
						ClassName = "TextLabel",
						BackgroundTransparency = 1,
						TextXAlignment = "Left",
						TextSize = 18,
						Font = Theme.Font,
						TextColor3 = Theme.Foreground,
						Text = opt,
						Size = UDim2.new(1, -8, 1, 0),
						Position = UDim2.new(0, 8, 0, 0),
					},
				})
			end
			menu = Sui {
				ClassName = "ScreenGui",
				ResetOnSpawn = false,
				ZIndexBehavior = "Sibling",
				DisplayOrder = 2147483647,
				Parent = game:GetService("Players").LocalPlayer:FindFirstChildOfClass("PlayerGui"),
				
				{
					ClassName = "TextButton",
					Text = "",
					BackgroundTransparency = 1,
					Size = UDim2.new(1, 0, 1, 0),
					Position = UDim2.new(0, 0, 0, 0),
					[Sui.event.MouseButton1Down] = function()
						close()
					end,
				},
				
				{
					ClassName = "DropshadowFrame",
					ZIndex = 2,
					Size = UDim2.new(props.Size.X, UDim.new(0, 8 + 32 * #props.Options)),
					Position = UDim2.new(0, refs.btn.AbsolutePosition.X, 0, refs.btn.AbsolutePosition.Y + refs.btn.AbsoluteSize.Y),
					BorderSizePixel = 0,
					BackgroundColor3 = Theme.Background,
					ShadowOffset = UDim2.new(0, 0, 0, 6),
					ClipsDescendants = true,
					ShadowTransparency = 0.6,
					Children = options,
				},
			}
		end,
		
		{
			ClassName = "TextLabel",
			Ref = "label",
			BackgroundTransparency = 1,
			Size = UDim2.new(1, -8, 1, 0),
			Position = UDim2.new(0, 8, 0, 0),
			Text = props.SelectedOption,
			Font = Theme.Font,
			TextSize = 16,
			TextColor3 = Theme.Foreground,
			TextXAlignment = "Left",
		},
		
		{
			ClassName = "ImageLabel",
			Ref = "arrow",
			Image = "rbxassetid://5112857665",
			ImageColor3 = Theme.Border,
			BackgroundTransparency = 1,
			Size = UDim2.new(0, 8, 0, 8),
			Position = UDim2.new(1, -12, 0.5, -4),
		},
		
		{
			ClassName = "Frame",
			Ref = "border",
			Size = UDim2.new(1, 0, 0, 2),
			Position = UDim2.new(0, 0, 1, -2),
			BorderSizePixel = 0,
			BackgroundColor3 = Theme.Border,
		},
	}
end

return Combobox