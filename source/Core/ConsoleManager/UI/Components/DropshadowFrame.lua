local Sui = require(script.Parent.Parent.Sui)

function DropshadowFrame(props, children)
	local frameProps = {
		Size = UDim2.new(0, 100, 0, 100),
		Position = UDim2.new(0, 0, 0, 0),
		ClassName = "Frame",
	}
	local shadowColor = Color3.new(0, 0, 0)
	local shadowTransparency = 0
	local shadowScale = 1
	local shadowOffset = UDim2.new(0, 0, 0, 0)
	for key, value in pairs(props) do
		if key == "ShadowColor" then
			shadowColor = value
		elseif key == "ShadowTransparency" then
			shadowTransparency = value
		elseif key == "ShadowOffset" then
			shadowOffset = value
		elseif key == "ShadowScale" then
			shadowScale = value
		elseif key == "InternalClass" then
			frameProps.ClassName = value
		elseif typeof(key) == "string" then
			frameProps[key] = value
		end
	end
	frameProps.ZIndex = (frameProps.ZIndex or 1) + 1
	frameProps.Children = children
	frameProps.Ref = "frame"
	local refs = {}
	local res = Sui(refs) {
		ClassName = "Folder",
		
		{
			ClassName = "ImageLabel",
			BackgroundTransparency = 1,
			Image = "http://www.roblox.com/asset/?id=5067576619",
			ScaleType = "Slice",
			SliceCenter = Rect.new(Vector2.new(58, 58), Vector2.new(59, 59)),
			ImageColor3 = shadowColor,
			ImageTransparency = shadowTransparency,
			ZIndex = frameProps.ZIndex - 1,
			Size = frameProps.Size + UDim2.new(0, 52 * shadowScale, 0, 52 * shadowScale),
			Position = frameProps.Position - UDim2.new(0, 26 * shadowScale, 0, 26 * shadowScale) + shadowOffset,
			SliceScale = shadowScale,
		},
		
		frameProps,
	}
	return res, refs.frame
end

return DropshadowFrame
