local Theme = require(script.Parent.Parent.Theme)
local Sui = require(script.Parent.Parent.Sui)

function Action(props)
	local refs = {}
	return Sui(refs) {
		ClassName = "TextButton",
		Text = "",
		BackgroundColor3 = Theme.Foreground,
		BackgroundTransparency = 1,
		BorderSizePixel = 0,
		Size = props.Size,
		Position = props.Position,
		Visible = props.Visible,
		[Sui.event.MouseEnter] = function()
			refs.circle:TweenSize(UDim2.new(1.5, 0, 1.5, 0), nil, nil, 0.075, true)
			refs.circle.ImageTransparency = Theme.HoverTransparency
		end,
		[Sui.event.MouseButton1Down] = function()
			refs.circle.ImageTransparency = Theme.HoverTransparency * 2 / 3
		end,
		[Sui.event.MouseButton1Up] = function()
			refs.circle.ImageTransparency = Theme.HoverTransparency
		end,
		[Sui.event.MouseLeave] = function()
			refs.circle:TweenSize(UDim2.new(0, 0, 0, 0), nil, nil, 0.075, true)
			refs.circle.ImageTransparency = Theme.HoverTransparency
		end,
		[Sui.event.MouseButton1Click] = function(self)
			local fn = props[Sui.event.Click]
			if fn then
				fn(self)
			end
		end,
		
		{
			ClassName = "ImageLabel",
			Ref = "circle",
			Image = "rbxassetid://1588248423",
			BackgroundTransparency = 1,
			Size = UDim2.new(0, 0, 0, 0),
			AnchorPoint = Vector2.new(0.5, 0.5),
			Position = UDim2.new(0.5, 0, 0.5, 0),
			ImageTransparency = 1,
			ImageColor3 = Theme.Foreground,
		},
		
		{
			ClassName = "ImageLabel",
			Name = "Image",
			Image = props.Image,
			BackgroundTransparency = 1,
			Size = UDim2.new(0, 24, 0, 24),
			Position = UDim2.new(0.5, -12, 0.5, -14),
			ImageColor3 = Theme.Foreground,
		},
		
		{
			ClassName = "TextLabel",
			Name = "Label",
			Text = props.Text:upper(),
			TextStrokeColor3 = Theme.Background,
			Font = Theme.Font,
			TextSize = 12,
			Size = UDim2.new(1, 0, 0, 12),
			Position = UDim2.new(0, 0, 1, -6),
			TextColor3 = Theme.Foreground,
			BackgroundTransparency = 1,
		},
	}
end

return Action