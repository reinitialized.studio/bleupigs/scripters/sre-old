local Players = game:GetService("Players")
local Sui = require(script.Sui)
local Tabs = require(script.Components.Tabs)
local Console = require(script.Pages.Console)
local Packages = require(script.Pages.Packages)
local Settings = require(script.Pages.Settings)
local Theme = require(script.Theme)

assert(require(script.Components.Registrar))

local UI = {}

local page

local mouseIn = false

local sliver = 24
local hitbox = 128

local function setMouseIn(m)
	local prev = mouseIn
	mouseIn = m
	if prev ~= mouseIn then
		UI.container:TweenPosition(UDim2.new(0, mouseIn and 0 or -634 + sliver, 1, -286 - 64), nil, nil, 0.4, true)
	end
end

game:GetService("UserInputService").InputChanged:Connect(function(input, gp)
	if input.UserInputType == Enum.UserInputType.MouseMovement and UI.container then
		local mpos = input.Position
		local apos = Vector2.new(math.max(UI.container.AbsolutePosition.X, -634 + hitbox), UI.container.AbsolutePosition.Y)
		local asize = UI.container.AbsoluteSize
		if mpos.X >= apos.X and mpos.X < apos.X + asize.X
			and mpos.Y >= apos.Y and mpos.Y < apos.Y + asize.Y then
			setMouseIn(true)
		elseif _G.Settings.get("autoHide") then
			setMouseIn(false)
		end
	end
end)

_G.Settings.onChanged("autoHide"):Connect(function(old, new)
	if not new then
		setMouseIn(true)
	end
end)

function UI.render()
	local refs = {}
	local res = Sui(refs) {
		ClassName = "ScreenGui",
		ResetOnSpawn = false,
		ZIndexBehavior = "Sibling",
		Parent = Players.LocalPlayer:FindFirstChildOfClass("PlayerGui"),
		
		{
			ClassName = "Frame",
			Ref = "container",
			Size = UDim2.new(0, 634, 0, 286),
			Position = UDim2.new(0, mouseIn and 0 or -634 + sliver, 1, -286 - 64),
			BackgroundTransparency = 1,
			
			{
				ClassName = "DropshadowFrame",
				Size = UDim2.new(1, 0, 1, 0),
				Ref = "mainFrame",
				BorderSizePixel = 0,
				BackgroundColor3 = Theme.Background,
				ShadowOffset = UDim2.new(0, 0, 0, 6),
				ClipsDescendants = true,
				ShadowTransparency = 0.6,
				
				Sui(refs) {
					ClassName = "ScriptControls",
					Ref = "scriptControls",
					Size = UDim2.new(1, -106, 0, 57),
					Position = UDim2.new(0, 106, 1, -23 - 57),
					Visible = require(script.Components.Scripts).getSelectedScript() ~= nil and true or false,
				},
				
				{
					ClassName = "Scripts",
					Size = UDim2.new(0, 105, 1, 0),
					Position = UDim2.new(0, 0, 0, 0),
				},
				
				{
					ClassName = "Frame",
					BorderSizePixel = 0,
					BackgroundColor3 = Theme.Border,
					Size = UDim2.new(0, 1, 1, 0),
					Position = UDim2.new(0, 105, 0, 0),
				},
				
				{
					ClassName = "Commandbar",
					Size = UDim2.new(1, -106, 0, 22),
					Position = UDim2.new(0, 106, 1, -22),
				},
				
				{
					ClassName = "Tabs",
					Size = UDim2.new(1, -106, 1, -22),
					Position = UDim2.new(0, 106, 0, 0),
					TabOrder = { "Console", --[["Packages",]] "Settings" },
					Console = Console.render(),
					Packages = Packages.render(),
					Settings = Settings.render(),
				},
			},
		},
	}
	UI.scriptControls = refs.scriptControls
	UI.container = refs.container
	UI.current = res
	return res
end

return UI