-- the Simple User Interface library
-- pronounced like the "swee" in "sweet"
local Sui = {}

local components = {}

function Sui.component(name, comp)
	components[name] = comp
end

Sui.event = setmetatable({}, {
	__index = function(self, key)
		local ev = { event = key }
		self[key] = ev
		return ev
	end,
})

local refs

setmetatable(Sui, {
	__call = function(_, data, childs)
		if not data.ClassName then
			return function(...)
				local save = refs
				refs = data
				local res = Sui(...)
				refs = save
				return res
			end
		end

		local class = data.ClassName
		local props = {}
		local children = childs or data.Children or {}
		for k, v in pairs(data) do
			if type(k) == "number" then
				table.insert(children, v)
			elseif k ~= "ClassName" and k ~= "Children" then
				props[k] = v
			end
		end
		local res
		if components[class] then
			local a, b = components[class](props, children)
			res = a
			if props.Ref then
				local v = props.Ref
				for rk, rv in pairs(type(v) == "string" and {v} or v) do
					if type(rv) == "function" then
						refs[rk] = function(...)
							return rv(b or res, ...)
						end
					elseif type(rk) == "number" then
						refs[rv] = b or res
					else
						refs[rk] = rv
					end
				end
			end
		else
			res = Instance.new(class)
			for k, v in pairs(props) do
				if type(k) == "table" then
					res[k.event]:Connect(function(...)
						v(res, ...)
					end)
				elseif k == "Ref" then
					for rk, rv in pairs(type(v) == "string" and {v} or v) do
						if type(rv) == "function" then
							refs[rk] = function(...)
								return rv(res, ...)
							end
						elseif type(rk) == "number" then
							refs[rv] = res
						else
							refs[rk] = rv
						end
					end
				elseif k ~= "Parent" then
					res[k] = v
				end
			end
			for _, v in ipairs(children) do
				v.Parent = res
				if typeof(v) ~= "Instance" then
					Sui(v)
				end
			end
		end
		res.Parent = props.Parent
		return res
	end,
})

return Sui