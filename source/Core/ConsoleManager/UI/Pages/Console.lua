local Sui = require(script.Parent.Parent.Sui)
local Theme = require(script.Parent.Parent.Theme)

local TextService = game:GetService("TextService")

local con = _G.Console

local Console = {}

function formatTime(when)
	local ms = (when % 1) * 1000
	local sec = math.floor(when) % 60
	local min = math.floor(when / 60) % 60
	local hr = math.floor(when / 3600) % 24
	return ("%02d:%02d:%02d.%03d"):format(hr, min, sec, ms)
end

Console.canvasPosition = Vector2.new()

function Console.render()
	local refs = {}
	
	local lineHeight = 18
	local outputHeight = 286 - 32 - 22
	local n = math.ceil(outputHeight / lineHeight) + 1
	local offset = 0
	
	local lines = {}
	for i = 1, n do
		table.insert(lines, {
			ClassName = "TextButton",
			Position = UDim2.new(0, 0, 0, (i - 1) * lineHeight),
			Size = UDim2.new(1, 0, 0, lineHeight - 1),
			BackgroundTransparency = 1,
			Text = "",
			[Sui.event.MouseEnter] = function()
				local lineTime = refs["lineTime" .. i]
				local line = refs["line" .. i]
				lineTime:TweenSize(UDim2.new(0, 57, 0, lineHeight), nil, nil, 0.15, true)
				line:TweenSizeAndPosition(UDim2.new(1, -61, 0, lineHeight), UDim2.new(0, 61, 0, (i - 1) * lineHeight), nil, nil, 0.15, true)
			end,
			[Sui.event.MouseLeave] = function()
				local lineTime = refs["lineTime" .. i]
				local line = refs["line" .. i]
				lineTime:TweenSize(UDim2.new(0, 0, 0, lineHeight), nil, nil, 0.15, true)
				line:TweenSizeAndPosition(UDim2.new(1, -4, 0, lineHeight), UDim2.new(0, 4, 0, (i - 1) * lineHeight), nil, nil, 0.15, true)
			end,
		})
		table.insert(lines, {
			ClassName = "TextLabel",
			Ref = {
				"lineTime" .. i,
				["updateTime" .. i] = function(self)
					local realIndex = i + offset
					local line = con.data[realIndex]
					if line then
						self.Text = formatTime(line.when)
					else
						self.Text = ""
					end
				end,
			},
			Position = UDim2.new(0, 4, 0, (i - 1) * lineHeight),
			Size = UDim2.new(0, 0, 0, lineHeight),
			TextColor3 = Theme.Console.Time,
			Font = Theme.Font,
			TextXAlignment = "Left",
			TextSize = 13,
			BackgroundTransparency = 1,
			Text = "",
			ClipsDescendants = true,
		})
		table.insert(lines, {
			ClassName = "TextLabel",
			Ref = {
				"line" .. i,
				["update" .. i] = function(self)
					local realIndex = i + offset
					local line = con.data[realIndex]
					local count = refs["count" .. i]
					if line then
						self.TextColor3 = Theme.Console[line.kind]
						self.Text = tostring(line.msg) .. " "
						if line.count <= 1 then
							count.Text = ""
							count.Visible = false
						else
							count.Text = " (x" .. line.count .. ")"
							local size = TextService:GetTextSize(self.Text, self.TextSize, self.Font, Vector2.new(10_000, 10_000))
							count.Position = UDim2.new(0, size.X, 0, 0)
							count.Visible = true
						end
					else
						self.Text = ""
					end
				end,
				["getSize" .. i] = function(self)
					local count = refs["count" .. i]
					return self.TextBounds.X + count.TextBounds.X + self.Position.X.Offset
				end,
			},
			Position = UDim2.new(0, 4, 0, (i - 1) * lineHeight),
			Size = UDim2.new(1, -4, 0, lineHeight),
			TextColor3 = Theme.Foreground,
			Font = Theme.Font,
			TextXAlignment = "Left",
			TextSize = 16,
			BackgroundTransparency = 1,
			Text = "",
			{
				ClassName = "TextLabel",
				Ref = {
					"count" .. i,
				},
				Position = UDim2.new(0, 0, 0, 0),
				Size = UDim2.new(1, 0, 0, lineHeight),
				TextColor3 = Theme.Console.Time,
				Font = Theme.Font,
				TextXAlignment = "Left",
				TextSize = 13,
				BackgroundTransparency = 1,
				Text = "",
			}
		})
	end
	
	local horizCanvasSize = 0
	local function updateAll()
		local maxSize = 0
		for i = 1, n do
			refs["updateTime" .. i]()
			refs["update" .. i]()
			maxSize = math.max(maxSize, refs["getSize" .. i]())
		end
		horizCanvasSize = maxSize
		refs.container.Size = UDim2.new(1, horizCanvasSize, 1, lineHeight)
		refs.update()
	end
	
	spawn(updateAll) -- TODO: super ugly but it works /shrug
	
	local function scrollToBottom()
		refs.scroller.CanvasPosition = Vector2.new(
			refs.scroller.CanvasPosition.X,
			refs.scroller.CanvasSize.Y.Offset - refs.scroller.AbsoluteWindowSize.Y)
	end
	
	con.event:Connect(function(data)
		local currentY = refs.scroller.CanvasPosition.Y
		refs.update()
		updateAll()
		if currentY + 20 >= math.max(0, refs.scroller.CanvasSize.Y.Offset - refs.scroller.AbsoluteWindowSize.Y) then
			scrollToBottom()
		end
	end)
	
	return Sui(refs) {
		ClassName = "Folder",
		
		{
			ClassName = "Frame",
			Ref = "container",
			BackgroundTransparency = 1,
			Size = UDim2.new(1, 0, 1, lineHeight),
			Children = lines,
		},
		
		{
			ClassName = "Action",
			Ref = "scrollToBottom",
			Position = UDim2.new(1, -48, 1, -48),
			Size = UDim2.new(0, 32, 0, 32),
			Text = "Scroll to\nBottom",
			Image = "rbxassetid://5119653458",
			Visible = false,
			[Sui.event.Click] = function(self)
				scrollToBottom()
			end,
		},
		
		{
			ClassName = "Scroller",
			Size = UDim2.new(1, 0, 1, 0),
			Position = UDim2.new(0, 0, 0, 0),
			Ref = {
				"scroller",
				update = function(self)
					self.CanvasSize = UDim2.new(0, horizCanvasSize, 0, #con.data * lineHeight)
				end,
			},
			CanvasSize = UDim2.new(0, 0, 0, #con.data * 18),
			CanvasPosition = Console.canvasPosition,
			[Sui.event.Changed] = function(self, prop)
				if prop == "CanvasPosition" then
					-- fix roblox bug:
					
					local maxHorizPos = math.max(0, refs.scroller.CanvasSize.X.Offset - refs.scroller.AbsoluteWindowSize.X)
					local horizPos = refs.scroller.CanvasPosition.X
					if horizPos > maxHorizPos then
						refs.scroller.CanvasPosition = Vector2.new(maxHorizPos, refs.scroller.CanvasPosition.Y)
						return
					end 
					
					-- end bugfix
					
					if refs.scroller.CanvasPosition.Y + 20 >= math.max(0, refs.scroller.CanvasSize.Y.Offset - refs.scroller.AbsoluteWindowSize.Y) then
						refs.scrollToBottom.Visible = false
					else
						refs.scrollToBottom.Visible = true
					end
					
					Console.canvasPosition = refs.scroller.CanvasPosition
					local y = refs.scroller.CanvasPosition.Y
					refs.container.Position = UDim2.new(0, -refs.scroller.CanvasPosition.X, 0, -(y % lineHeight))
					offset = math.floor(y / lineHeight)
					updateAll()
				end
			end,
		},
	}
end

return Console