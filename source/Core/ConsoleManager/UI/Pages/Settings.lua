local Sui = require(script.Parent.Parent.Sui)
local Theme = require(script.Parent.Parent.Theme)

local Console = {}

function setting(layoutOrder, name, displayName, changer)
	return Sui {
		ClassName = "Frame",
		BackgroundTransparency = 1,
		Size = UDim2.new(1, 0, 0, 24),
		
		{
			ClassName = "TextLabel",
			BackgroundTransparency = 1,
			Size = UDim2.new(1, 0, 1, 0),
			TextXAlignment = "Left",
			TextSize = 18,
			Font = Theme.Font,
			TextColor3 = Theme.Foreground,
			Text = displayName,
		},
		
		changer,
	}
end

function Console.render()
	return Sui {
		ClassName = "Frame",
		BackgroundTransparency = 1,
		Size = UDim2.new(1, -32, 1, -16),
		Position = UDim2.new(0, 16, 0, 8),
		
		{
			ClassName = "UIListLayout",
			Padding = UDim.new(0, 8),
			SortOrder = "LayoutOrder",
		},
		
		setting(1, "promptLocals", "Prompt Locals", Sui {
			ClassName = "Combobox",
			Size = UDim2.new(0, 128, 0, 24),
			Position = UDim2.new(1, -128, 0, 0),
			SelectedOption = _G.Settings.get("promptLocals") and "Yes" or "No",
			Options = {
				"Yes",
				"No",
			},
			[Sui.event.Select] = function(_, value)
				_G.Settings.set("promptLocals", value == "Yes")
			end,
		}),
		
		setting(2, "autoHide", "Auto-Hide Output", Sui {
			ClassName = "Combobox",
			Size = UDim2.new(0, 128, 0, 24),
			Position = UDim2.new(1, -128, 0, 0),
			SelectedOption = _G.Settings.get("autoHide") and "Yes" or "No",
			Options = {
				"Yes",
				"No",
			},
			[Sui.event.Select] = function(_, value)
				_G.Settings.set("autoHide", value == "Yes")
			end,
		}),
		
		setting(3, "theme", "Theme", Sui {
			ClassName = "Combobox",
			Size = UDim2.new(0, 128, 0, 24),
			Position = UDim2.new(1, -128, 0, 0),
			SelectedOption = _G.Settings.get("theme"),
			Options = {
				"Light",
				"Dark",
			},
			[Sui.event.Select] = function(_, value)
				_G.Settings.set("theme", value)
			end,
		}),
	}
end

return Console