local currTheme = function()
	if _G.Settings.get("theme") == "Light" then
		return {
			Background = Color3.fromRGB(0xff, 0xff, 0xff),
			Foreground = Color3.fromRGB(0x00, 0x00, 0x00),
			Border = Color3.fromRGB(0xce, 0xd4, 0xda),
			Accent = Color3.fromRGB(0x4d, 0xab, 0xf7),
			AccentForeground = Color3.fromRGB(0xf8, 0xf9, 0xfa),
			Font = "SourceSans",
			HoverTransparency = 0.86,
			Console = {
				Time = Color3.fromRGB(0x86, 0x8e, 0x96),
				Print = Color3.fromRGB(0x00, 0x00, 0x00),
				Error = Color3.fromRGB(0xf0, 0x3e, 0x3e),
				Info = Color3.fromRGB(0x1c, 0x7e, 0xd6),
				SB = Color3.fromRGB(0x2b, 0x8a, 0x3e),
			},
			Scripts = {
				NormalStopped = Color3.fromRGB(0x00, 0x00, 0x00),
				SavedStopped = Color3.fromRGB(0x00, 0x00, 0x00),
				NormalRunning = Color3.fromRGB(0x2b, 0x8a, 0x3e),
				SavedRunning = Color3.fromRGB(0x2b, 0x8a, 0x3e),
			},
		}
	else -- Dark
		return {
			Background = Color3.fromRGB(0x21, 0x25, 0x29),
			Foreground = Color3.fromRGB(0xf8, 0xf9, 0xfa),
			Border = Color3.fromRGB(0x49, 0x50, 0x57),
			Accent = Color3.fromRGB(0x4d, 0xab, 0xf7),
			AccentForeground = Color3.fromRGB(0xf8, 0xf9, 0xfa),
			Font = "SourceSans",
			HoverTransparency = 0.7,
			Console = {
				Time = Color3.fromRGB(0xad, 0xb5, 0xbd),
				Print = Color3.fromRGB(0xf8, 0xf9, 0xfa),
				Error = Color3.fromRGB(0xfa, 0x52, 0x52),
				Info = Color3.fromRGB(0x4d, 0xab, 0xf7),
				SB = Color3.fromRGB(0x51, 0xcf, 0x66),
			},
			Scripts = {
				NormalStopped = Color3.fromRGB(0xf8, 0xf9, 0xfa),
				SavedStopped = Color3.fromRGB(0xf8, 0xf9, 0xfa),
				NormalRunning = Color3.fromRGB(0x51, 0xcf, 0x66),
				SavedRunning = Color3.fromRGB(0x51, 0xcf, 0x66),
			},
		}
	end
end

return setmetatable({}, {
	__index = function(_, key)
		return currTheme()[key]
	end,
})