local Settings = {}

local settingData = {
	promptLocals = true,
	autoHide = true,
	theme = "Dark",
}

local events = {}

function Settings.onChanged(setting)
	local res = events[setting]
	if not res then
		res = Instance.new("BindableEvent")
		events[setting] = res
	end
	return res.Event
end

function Settings.get(name)
	return settingData[name]
end

function Settings.set(name, value)
	assert(settingData[name] ~= nil)
	assert(typeof(value) == typeof(settingData[name]))
	if settingData[name] ~= value then
		local ev = events[name]
		local old = settingData[name]
		settingData[name] = value
		if ev then
			ev:Fire(old, value)
		end
	end
end

_G.Settings = Settings

return Settings