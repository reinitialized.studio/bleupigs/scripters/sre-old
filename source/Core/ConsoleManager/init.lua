--[[

Documentation:

require(script.API):

RBXScriptSignal API.onCommand(string cmd)
	Fired when a command is executed by the user in the commandbar

RBXScriptSignal API.onToggleRun(var id)
	Fired when a script's runtime is toggled

RBXScriptSignal API.onToggleSave(var id)
	Fired when a script's saved state is toggled

function API.runCommand(string cmd)
	Call to run a command on the user's behalf

function API.toggleRun(var id)
	Call to toggle a script's runtime

function API.toggleSave(var id)
	Call to toggle a script's saved state

_G.Console:

type MsgKind = "Print" | "Error" | "Info" | "SB"

function Console.print(MsgKind kind, string msg, double when)
	Prints a message of the given kind to the console at the given Unix time.

function Console.clear()
	Clears the console

_G.Scripts:

enum State
	Scripts.NORMAL
	Scripts.SAVED
	Scripts.RUNNING
	Scripts.QUICK
	
	May be added together to represent multiple flags at once.
	For example, a running quickscript would use (Scripts.RUNNING + Scripts.QUICK)

function Scripts.add(var id, string name, State state)
	Adds a script with the given id, name, and state. The id may be of any serializable type.
	Both the id and name shall be unique.

function Scripts.list()
	Returns a list of all script IDs in the database, in no particular order.

function Scripts.remove(var id)
	Removes the script with the given id, if it exists.

function Scripts.getId(string name)
	Gets the id of a script from its name.

function Scripts.getName(var id)
	Gets the name of a script from its id.

function Scripts.matches(var id, State mask)
	Checks if a script matches all values in the given state mask.

function Scripts.addState(var id, State state)
	Adds the given state (or states) to the script.

function Scripts.removeState(var id, State state)
	Removes the given state (or states) from the script.

_G.Settings:

function Settings.onChanged(string setting)
	Returns an RBXScriptSignal that is fired when the given setting is changed.

function Settings.get(string name)
	Gets the value of the setting with the given name.

function Settings.set(string name, var value)
	Sets the value of the setting with the given name.

Available settings:
	bool promptLocals = true
	string theme = "Dark"

Settings are not saved. This should be handled outside this script.

]]

function initUI()
	require(script.Console)
	require(script.Scripts)
	require(script.Settings)
	
	local UI = require(script.UI)
	
	local oldUi = UI.render()
	
	_G.Settings.onChanged("theme"):Connect(function()
		oldUi:Destroy()
		oldUi = UI.render()
	end)
end

return initUI